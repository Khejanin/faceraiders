﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

public class TapToDestroy : MonoBehaviour
{

    private ARSessionOrigin _arSessionOrigin;
    private ARRaycastManager _arRaycastManager;
    
    // Start is called before the first frame update
    void Start()
    {
        _arSessionOrigin = FindObjectOfType<ARSessionOrigin>();
        _arSessionOrigin.GetComponent<ARRaycastManager>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Began)
        {
            Shoot();
        }
    }

    void Shoot()
    {
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out RaycastHit hit,
            float.PositiveInfinity))
        {
            if (hit.collider.CompareTag("Enemy"))
            {
                hit.collider.gameObject.GetComponent<Enemy>().Die();
            }
        }
    }
    
}
