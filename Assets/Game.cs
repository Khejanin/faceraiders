﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Game : MonoBehaviour
{

    [SerializeField] private GameObject enemy;
    [SerializeField] private int nrOfEnemies = 20;
    [SerializeField] private TextMeshProUGUI scoreText;

    private int score = 0;
    
    private int xSpawnMin = -3;
    private int xSpawnMax = 3;
    
    private int zSpawnMin = 1;
    private int zSpawnMax = 3;

    private List<GameObject> enemies;
    
    // Start is called before the first frame update
    void Start()
    {
        scoreText.SetText("Score : " + score);
        for (int i = 0; i < 3; i++)
        {
            SpawnEnemy();
        }
    }

    public delegate void OnDeath();
    
    void OnEnemyDeath()
    {
        score++;
        scoreText.SetText("Score : " + score);
        SpawnEnemy();
    }

    void SpawnEnemy()
    {
        var e = Instantiate(enemy, GenerateEnemySpawnPos(), Quaternion.identity);
        e.GetComponent<Enemy>().SetCallback(OnEnemyDeath);
    }

    Vector3 GenerateEnemySpawnPos()
    {
        return new Vector3(transform.position.x + Random.Range(xSpawnMin, xSpawnMax+1), transform.position.y,
            transform.position.z + Random.Range(zSpawnMin, zSpawnMax+1) * (Random.Range(0f, 1) < 0.5f ? -1 : 1));
    }
    
}
