﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEditor;
using UnityEngine;
using Random = UnityEngine.Random;

public class Enemy : MonoBehaviour
{
    
    [SerializeField] private int xTargetMin = -3;
    [SerializeField] private int xTargetMax = 3;
    
    [SerializeField] private int yTargetMin = -3;
    [SerializeField] private int yTargetMax = 3;
    
    [SerializeField] private float speed = 1f;
    public GameObject onDeathParticleSystem;
    
    private Vector3 relativeRoot;
    private Vector3 currentTargetPos;
    

    private Game.OnDeath gameOnDeathCallback;
    
    private float minTargetDistance = 0.1f;
    
    private void Start()
    {
        relativeRoot = transform.position;
        GenerateNewTargetPos();
        LookAtPlayer();
    }

    public void SetCallback(Game.OnDeath cb)
    {
        gameOnDeathCallback = cb;
    }
    
    void Update()
    {
        GoToTargetPos();
        if(Arrived()) GenerateNewTargetPos();
    }

    void LookAtPlayer()
    {
        transform.LookAt(Camera.main.transform);
    }

    void GoToTargetPos()
    {
        transform.position = Vector3.MoveTowards(transform.position, currentTargetPos, speed * Time.deltaTime);
    }
    
    bool Arrived()
    {
        return Vector3.Distance(transform.position, currentTargetPos) < minTargetDistance;
    }

    void GenerateNewTargetPos()
    {
        //Z is locked
        currentTargetPos = new Vector3(Camera.main.transform.position.x + relativeRoot.x + Random.Range(xTargetMin,xTargetMax),Camera.main.transform.position.y + relativeRoot.y + Random.Range(yTargetMin,yTargetMax) , Camera.main.transform.position.z + relativeRoot.z);
    }

    public void Die()
    {
        gameOnDeathCallback.Invoke();
        GameObject p = Instantiate(onDeathParticleSystem, transform.position, transform.rotation);
        Destroy(p,p.GetComponent<ParticleSystem>().main.duration);
        Destroy(gameObject);
    }
    
}
